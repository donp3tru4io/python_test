from __future__ import unicode_literals
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import (
    AbstractBaseUser, PermissionsMixin
)
from .managers import UserManager


class User(AbstractBaseUser, PermissionsMixin):
    ROLES = (
        (0, "USER"),
        (1, "STAFF"),
        (2, "ADMIN"),
    )
    email = models.EmailField(unique=True)
    name = models.CharField(max_length=30)
    s_name = models.CharField(max_length=30)
    password = models.CharField(max_length=200)
    number = models.CharField(max_length=15)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    role = models.SmallIntegerField(default=0, choices=ROLES)
    created = models.DateTimeField(default=timezone.now)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name', 's_name']

    def save(self, *args, **kwargs):
        super(User, self).save(*args, **kwargs)
        return self

    class Meta:
        permissions = (
            ("USER", "user"),
            ("STAFF", "staff"),
            ("ADMIN", "admin"),
        )


class Service(models.Model):
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=100)


class Order(models.Model):
    STATUS = (
        (0, "ORDERED"),
        (1, "REGISTERED"),
        (2, "DONE"),
    )
    customer_name = models.CharField(max_length=30)
    customer_s_name = models.CharField(max_length=30)
    customer_email = models.EmailField()
    customer_number = models.CharField(max_length=15)
    description = models.CharField(max_length=10)
    staff = models.ForeignKey(User, on_delete=models.CASCADE, related_name='staff')
    customer = models.ForeignKey(User, on_delete=models.CASCADE, related_name='customer')
    created = models.DateTimeField()
    service = models.ForeignKey(Service, on_delete=models.CASCADE)
    status = models.SmallIntegerField(default=0, choices=STATUS)
