from rest_framework import permissions


class HasAuthorities(permissions.BasePermission):
    def has_permission(self, request, view):

        user = request.user

        if not user.is_authenticated:
            return False

        print(user.get_all_permissions())

        path = request.path

        if str(path).startswith('/api/user/'):
            return True

        if str(path).startswith('/api/staff/'):
            return user.has_perm('api.STAFF')

        if str(path).startswith('/api/admin/'):
            return user.has_perm('api.ADMIN')

        return False
