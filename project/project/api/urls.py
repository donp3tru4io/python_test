from django.urls import path

from project.api.api_views import anonymous, user, admin
from . import views

urlpatterns = (
    path('test/', views.test, name='test'),

    path('free/registration/', anonymous.register, name='registration'),
    path('free/login/', anonymous.authenticate_user, name='user_login'),
    path('free/test/', anonymous.test, name='anon_test'),

    path('user/get_info/', user.get_info, name='user_get_info'),
    path('user/test/', anonymous.test, name='user_test'),

    path('admin/test/', admin.test, name='admin_test'),
    path('admin/create_staff/', admin.create_staff, name='admin_create_staff'),
)
