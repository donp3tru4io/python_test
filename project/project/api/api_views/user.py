from django.http import JsonResponse
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated

from project.api.serializer import UserSerializer

#curl -X GET -H "Content-Type: application/json" "Authorization: Bearer " http://localhost:9696/api/admin/test/
@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
def test(request):
    return JsonResponse({"message": "ok"}, status=status.HTTP_200_OK)

#curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer " http://localhost:9696/api/user/info/
@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
def get_info(request):
    serializer = UserSerializer(request.user)
    return JsonResponse(serializer.data, status=status.HTTP_200_OK)