import jwt
from django.contrib.auth import user_logged_in
from django.contrib.auth.models import Permission
from django.http import JsonResponse
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework_jwt.serializers import jwt_payload_handler

from project import settings
from project.api.models import User
from project.api.serializer import UserSerializer

@api_view(['GET'])
def test(request):
    return JsonResponse({'message': 'ok'})


#curl -X POST -H "Content-Type: application/json" http://localhost:9696/api/user/login/ -d '{"email":"admin@admin.com", "password": "password"}'
@api_view(['POST'])
def authenticate_user(request):
    try:
        email = request.data['email']
        password = request.data['password']
        user = User.objects.get(email=email, password=password)
        if user:
            try:
                payload = jwt_payload_handler(user)
                token = jwt.encode(payload, settings.SECRET_KEY).decode("utf-8")
                user_details = {}
                user_details['name'] = " %s %s " % (user.name, user.s_name)
                user_details['token'] = token
                user_logged_in.send(sender=user.__class__, request= request, user=user)
                return JsonResponse(user_details, status=status.HTTP_200_OK)
            except Exception as e:
                raise e
        else:
            return JsonResponse({'error': 'cannot authenticate'}, status=status.HTTP_401_UNAUTHORIZED)
    except User.DoesNotExist:
        return JsonResponse({'error': 'wrong credentials'}, status=status.HTTP_401_UNAUTHORIZED)
    except KeyError:
        return JsonResponse({'error': 'wrong credentials'}, status=status.HTTP_401_UNAUTHORIZED)

#curl -X POST -H "Content-Type: application/json" http://localhost:9696/api/registration/ -d '{"email":"don@gmail.com", "name": "Don", "s_name": "P3tru4io", "password": "password", "number": "0674592031"}'
@api_view(['POST'])
def register(request):
    try:
        user = User.objects.get(email=request.data['email'])

    except User.DoesNotExist:
        serializer = UserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        user = User.objects.get(email=request.data['email'])
        user.user_permissions.add(Permission.objects.get(name="user"))
        return JsonResponse ({'message': 'ok'}, status=status.HTTP_200_OK)
    return JsonResponse ({'error': 'email is busy'}, status=status.HTTP_400_BAD_REQUEST)


