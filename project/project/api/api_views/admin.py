from django.contrib.auth.models import Permission
from django.http import JsonResponse
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated

from project.api.auth.auth_manager import HasAuthorities
from project.api.models import User
from project.api.serializer import UserSerializer


#curl -X GET -H "Content-Type: application/json" "Authorization: Bearer " http://localhost:9696/api/admin/test/
@api_view(['GET'])
@permission_classes((IsAuthenticated, HasAuthorities))
def test(request):
    return JsonResponse({"message": "ok"}, status=status.HTTP_200_OK)

#curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer " http://localhost:9696/api/admin/create_staff/ -d '{"email":"staff@staff.com", "name": "staff", "s_name": "STAFF", "password": "password", "number": "000000000000"}'
@api_view(['POST'])
@permission_classes((IsAuthenticated, HasAuthorities))
def create_staff(request):
    new_staff = request.data
    try:
        user = User.objects.get(email=new_staff['email'])
    except User.DoesNotExist:
        new_staff['role'] = 1
        serializer = UserSerializer(data=new_staff)
        serializer.is_valid()
        serializer.save()
        user = User.objects.get(email=new_staff['email'])
        user.user_permissions.add(Permission.objects.get(name="staff"))
        return JsonResponse ({'message': 'ok'}, status=status.HTTP_200_OK)
    return JsonResponse({'error': 'email busy'}, status=status.HTTP_200_OK)
