from django.contrib.auth.models import Permission
from django.db import migrations

from project.api.models import User


def add_permission(apps, schema_editor):
    user = User.objects.get(email='admin@admin.com')
    user.user_permissions.add(Permission.objects.get(name='admin'))

class Migration(migrations.Migration):

    initial = False

    dependencies = [
        ('api', '0002_services'),
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(add_permission)
    ]