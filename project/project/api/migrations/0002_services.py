from django.db import migrations


class Migration(migrations.Migration):

    initial = False

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.RunSQL(
            "INSERT INTO api_service (name, description) VALUES ('service0', 'some service 0'), ('service1', 'some service 1'), ('service2', 'some service 2')",
        ),
        migrations.RunSQL(
            "INSERT INTO api_user (is_superuser, email, name, s_name, password, number, role, is_active, is_staff, created) VALUE (True, 'admin@admin.com', 'Admin', 'ADMIN', 'password', '1234567890', 2, True, True, '2019-05-02 11:59:56.684773')"
        )
    ]