from rest_framework import serializers
from .models import User, Order, Service

class UserSerializer(serializers.ModelSerializer):

    created = serializers.ReadOnlyField()

    class Meta(object):
        model = User
        fields = ('id', 'email', 'name', 's_name',
                  'password', 'number', 'role', 'created')
        extra_kwargs = {'password': {'write_only': True}}


class OrderSerializer(serializers.ModelSerializer):

    created = serializers.ReadOnlyField()

    class Meta(object):
        model = Order
        fields = ('id', 'service', 'customer_name', 'customer_s_name', 'customer_email', 'customer_number',
                  'description', 'staff', 'customer', 'created')


class ServiceSerializer(serializers.ModelSerializer):

    class Meta(object):
        model = Service
        fields = ('id', 'name', 'description')